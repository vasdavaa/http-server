import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.StringTokenizer;

public class JavaHTTPServer implements Runnable {

	static final File WEB_ROOT = new File(".");
	static final String DEFAULT_FILE = "index.html";
	static final String FILE_NOT_FOUND = "404.html";

	private Socket connect;

	public JavaHTTPServer(Socket c) {
		connect = c;
	}

	public static void main(String[] args) {
		try {
			ServerSocket serverConnect = new ServerSocket(8080);
			System.out.println("Server started.\nListening for connections on port : 8080 ...\n");

			while (true) {
				JavaHTTPServer myServer = new JavaHTTPServer(serverConnect.accept());

				Thread thread = new Thread(myServer);
				thread.start();
			}
		} catch (IOException e) {
			System.err.println("Server Connection error : " + e.getMessage());
		}
	}

	@Override
	public void run() {
		BufferedReader in = null;
		BufferedOutputStream dataOut = null;
		String fileRequested = null;

		try {
			in = new BufferedReader(new InputStreamReader(connect.getInputStream()));
			dataOut = new BufferedOutputStream(connect.getOutputStream());

			String input = in.readLine();

			StringTokenizer parse = new StringTokenizer(input);
			String method = parse.nextToken().toUpperCase(); // we get the HTTP method of the client

			// we get file requested
			fileRequested = parse.nextToken().toLowerCase();

			// we support only GET and HEAD methods, we check
			if (method.equals("GET") || method.equals("HEAD")) {
				// GET or HEAD method
				if (fileRequested.endsWith("/")) {
					fileRequested += DEFAULT_FILE;
				}

				if (method.equals("GET")) { // GET method so we return content
					sendHTTPResponse(new File(WEB_ROOT, fileRequested), false, dataOut);
				}
			}

		} catch (FileNotFoundException fnfe) {
			try {
				fileNotFound(dataOut);
			} catch (IOException ioe) {
				System.err.println("Error with file not found exception : " + ioe.getMessage());
			}

		} catch (IOException ioe) {
			System.err.println("Server error : " + ioe);
		} finally {
			try {
				in.close();
				dataOut.close();
				connect.close(); // we close socket connection
			} catch (Exception e) {
				System.err.println("Error closing stream : " + e.getMessage());
			}
		}
	}

	private byte[] readFileData(File file, int fileLength) throws IOException {
		FileInputStream fileIn = null;
		byte[] fileData = new byte[fileLength];

		try {
			fileIn = new FileInputStream(file);
			fileIn.read(fileData);
		} finally {
			if (fileIn != null)
				fileIn.close();
		}

		return fileData;
	}

	private void sendHTTPResponse(File file, boolean isFileFound,
			BufferedOutputStream outToClient)
			throws IOException {
		String fileRequested = file.getName();
		String contentType = getContentType(fileRequested);
		System.out.println("Requested file: " + fileRequested + " " + contentType);

		int fileLength = (int) file.length();
		byte[] fileData = readFileData(file, fileLength);

		outToClient.write("HTTP/1.1 200 OK\r\n".getBytes());
		outToClient.write(("Content-Type: " + contentType + "\r\n").getBytes());
		outToClient.write(("Server: Java HTTP Server v1.0").getBytes());
		outToClient.write(("Date: " + new Date()).getBytes());
		outToClient.write(("Content-Length: " + fileLength + "\r\n").getBytes());
		outToClient.write("\r\n".getBytes());

		outToClient.write(fileData, 0, fileLength);
		outToClient.flush();
	}

	// return supported MIME Types
	private String getContentType(String fileRequested) {
		if (fileRequested.endsWith(".htm") || fileRequested.endsWith(".html")) {
			return "text/html";
		} else if (fileRequested.endsWith(".jpg")) {
			return "image/jpeg";
		} else {
			return "text/plain";
		}
	}

	private void fileNotFound(BufferedOutputStream dataOut) throws IOException {
		System.out.println("File not found");
		sendHTTPResponse(new File(WEB_ROOT, FILE_NOT_FOUND), true, dataOut);
	}

}